#!/bin/bash

# Make dirs for 90dns static files
sudo mkdir /var/www/html/90dns && cd /var/www/html/90dns

# write HTML contents
printf '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><title>HTML Page</title></head><body bgcolor="#FFFFFF">This is test.html page</body></html>' | sudo tee conntest.nintendowifi.net.html
printf 'ok' | sudo tee ctest.cdn.nintendo.net.txt
printf 'You are successfully connected to 90DNS. Happy hacking!' | sudo tee 90dns.test.html

# modify lighttpd configuration, Pihole recommands to modify settings through external.conf
cat <<-'EOF' | sudo tee -a /etc/lighttpd/external.conf && sudo systemctl restart lighttpd.service
# https://redmine.lighttpd.net/boards/2/topics/6541
var.90dns-response-headers = (
    "X-Organization" => "Nintendo"
)

# https://stackoverflow.com/a/32128324/1043209
$HTTP["host"] == "conntest.nintendowifi.net" {
    url.rewrite-once = ( ".*" => "/90dns/conntest.nintendowifi.net.html" )
    setenv.add-response-header = var.90dns-response-headers
}

$HTTP["host"] == "ctest.cdn.nintendo.net" {
    url.rewrite-once = ( ".*" => "/90dns/ctest.cdn.nintendo.net.txt" )
    setenv.add-response-header = var.90dns-response-headers
    setenv.add-response-header += ( "Content-Type" => "text/plain" )
}

$HTTP["host"] =~ "90dns.test$" {
    url.rewrite-once = ( ".*" => "/90dns/90dns.test.html" )
    setenv.add-response-header = var.90dns-response-headers
}
EOF

# Change 90dns IP to self hosted one
# if `dnsutils` is not installed in your Pi, you need to modify the config manually
command -v dig && sudo sed -i -e "s/95.216.149.205/$(dig +short pi.hole)/" /etc/dnsmasq.d/03-90dns.conf && sudo systemctl restart pihole-FTL.service || echo "[ERROR] you need to replace 90dns IP (95.216.149.205) with your pi.hole IP inside /etc/dnsmasq.d/03-90dns.conf"
