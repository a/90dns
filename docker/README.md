# Dockerfile for 90dns

To build the image, run this command within this (current) folder:

```
docker build -t 90dns .
```

To run the image, run this command:

```
docker run -d --name 90dns -p 53:53/tcp -p 53:53/udp 90dns
```
