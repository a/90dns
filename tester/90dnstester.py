#!/usr/bin/python3
import sys
from typing import List

try:
    from dns import resolver  # dnspython
except ModuleNotFoundError:
    print("You need dnspython to use this script.")
    print("Run 'pip install dnspython' to install it.")
    sys.exit()

print("90DNS Tester by aveao/AveSatanas, released under GPLv2.")

dns_servers = ["163.172.141.219", "207.246.121.77"]

# Initialize resolver, set up the DNS servers
dns_resolver = resolver.Resolver()

# Define a basic dataset, with a list of the most important domains
test_dataset = {
    # Nintendo
    "nintendo.net": ["127.0.0.1"],
    "sun.hac.lp1.d4c.nintendo.net": ["127.0.0.1"],
    "nintendo.com": ["127.0.0.1"],
    "ctest.cdn.nintendo.net": ["207.246.121.77", "95.216.149.205"],
    "conntest.nintendowifi.net": ["207.246.121.77", "95.216.149.205"],
    # PS4
    "gs2.sonycoment.loris-e.llnwd.net": ["127.0.0.1"],
    "gs2.ww.prod.dl.playstation.net.edgesuite.net": ["127.0.0.1"],
    "heu01.ps4.update.playstation.net": ["127.0.0.1"],
    "sie-rd.com": ["127.0.0.1"],
    "sonyentertainmentnetwork.com": ["127.0.0.1"],
    "scea.com": ["127.0.0.1"],
    "playstation.com": ["127.0.0.1"],
    "get.net.playstation.net": ["127.0.0.1"],
    "post.net.playstation.net": ["127.0.0.1"],
    "ena.net.playstation.net": ["127.0.0.1"],
    "update.net.playstation.net": ["127.0.0.1"],
    "manuals.playstation.net": ["127.0.0.1"],
    # Test domain
    "90dns.test": ["207.246.121.77", "95.216.149.205"],
}

test_dataset_ipv6 = {
    # Nintendo
    "nintendo.net": ["::"],
    "sun.hac.lp1.d4c.nintendo.net": ["::"],
    "nintendo.com": ["::"],
    # PS4
    "gs2.sonycoment.loris-e.llnwd.net": ["::"],
    "gs2.ww.prod.dl.playstation.net.edgesuite.net": ["::"],
    "heu01.ps4.update.playstation.net": ["::"],
    "sie-rd.com": ["::"],
    "sonyentertainmentnetwork.com": ["::"],
    "scea.com": ["::"],
    "playstation.com": ["::"],
    "get.net.playstation.net": ["::"],
    "post.net.playstation.net": ["::"],
    "ena.net.playstation.net": ["::"],
    "update.net.playstation.net": ["::"],
    "manuals.playstation.net": ["::"],
}


def compare_dns(domain_to_test: str, expected_ips: List[str], query_type: str = "A"):
    # Query a domain's A/AAAA records, convert results to strings
    # and check if it's in the list of expected IPs
    try:
        dns_results = dns_resolver.resolve(domain_to_test, query_type)
        for dns_result in dns_results:
            if str(dns_result) not in expected_ips:
                dns_results_str = [str(dns_res_raw) for dns_res_raw in list(dns_results)]
                print(f"Got results {dns_results_str} from {domain_to_test}.")
                return False
        return True
    except (resolver.NoAnswer, resolver.NXDOMAIN):
        # No answer or no domain, fail
        return ""


print("Starting tests now.\n")

any_failed = False

for dns_server in dns_servers:
    print(f"Testing {dns_server}:")
    dns_resolver.nameservers = [dns_server]

    test_successes = 0
    for test_domain, expected_ips in test_dataset.items():
        if not (compare_dns(test_domain, expected_ips, "A")):
            print(f"Incorrect records detected on {test_domain} (IPv4).")
            any_failed = True
        else:
            test_successes += 1
            print(f"All good on {test_domain} (IPv4).")

    print(f"\n{test_successes}/{len(test_dataset)} IPv4 queries had the expected result.\n")

    test_successes = 0
    for test_domain, expected_ips in test_dataset_ipv6.items():
        if not (compare_dns(test_domain, expected_ips, "AAAA")):
            print(f"Incorrect records detected on {test_domain} (IPv6).")
            any_failed = True
        else:
            test_successes += 1
            print(f"All good on {test_domain} (IPv6).")

    print(f"\n{test_successes}/{len(test_dataset_ipv6)} IPv6 queries had the expected result.\n")

# If all tests succeeded, notify user of that.
if not any_failed:
    print("It should be safe to use 90DNS on this network.")
else:
    print(
        """It is NOT safe to use 90DNS on this network.
Try setting up your own 90DNS instance:
https://gitlab.com/a/90dns/blob/master/SELFHOST.md"""
    )
